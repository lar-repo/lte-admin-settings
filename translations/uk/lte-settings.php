<?php

return [
    'menu_title' => 'Налаштування',
    'field' => 'Поле',
    'key' => 'Ключ',
    'description' => 'Опис',
    'active' => 'Активний',
    'value' => 'Значення',
    'inputs' => [
        'input' => 'Звичайне введення',
        'password' => 'Пароль',
        'email' => 'Електронна пошта',
        'number' => 'Число',
        'file' => 'Файл',
        'switcher' => 'Перемикач',
        'date' => 'Дата',
        'time' => 'Час',
        'icon' => 'Іконка',
        'color' => 'Колір',
        'select_tags' => 'Теги',
        'ckeditor' => 'CkEditor',
        'textarea' => 'Введення тексту'
    ]
];