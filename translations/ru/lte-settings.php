<?php

return [
    'menu_title' => 'Настройки',
    'field' => 'Поле',
    'key' => 'Ключ',
    'description' => 'Описание',
    'active' => 'Активный',
    'value' => 'Значение',
    'inputs' => [
        'input' => 'Обычный ввод',
        'password' => 'Пароль',
        'email' => 'Электронная почта',
        'number' => 'Число',
        'file' => 'Файл',
        'switcher' => 'Переключатель',
        'date' => 'Дата',
        'time' => 'Время',
        'icon' => 'Иконка',
        'color' => 'Цвет',
        'select_tags' => 'Теги',
        'ckeditor' => 'CkEditor',
        'textarea' => 'Ввод текста'
    ]
];