<?php

return [
    'menu_title' => 'Settings',
    'field' => 'Field',
    'key' => 'Key',
    'description' => 'Description',
    'active' => 'Active',
    'value' => 'Value',
    'inputs' => [
        'input' => 'Input',
        'password' => 'Password',
        'email' => 'E-Mail',
        'number' => 'Number',
        'file' => 'File',
        'switcher' => 'Switcher',
        'date' => 'Date',
        'time' => 'Time',
        'icon' => 'Icon',
        'color' => 'Color',
        'select_tags' => 'Tags',
        'ckeditor' => 'CkEditor',
        'textarea' => 'Text area'
    ]
];