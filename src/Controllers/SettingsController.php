<?php

namespace Lar\LteAdmin\Extend\Settings\Controllers;

use Lar\LteAdmin\Controllers\Controller;
use Lar\LteAdmin\Extend\Settings\Models\LteSettings;
use Lar\LteAdmin\Segments\Info;
use Lar\LteAdmin\Segments\Matrix;
use Lar\LteAdmin\Segments\Sheet;
use Lar\LteAdmin\Segments\Tagable\Card;
use Lar\LteAdmin\Segments\Tagable\Field;
use Lar\LteAdmin\Segments\Tagable\Form;
use Lar\LteAdmin\Segments\Tagable\ModelInfoTable;
use Lar\LteAdmin\Segments\Tagable\ModelTable;

/**
 * Class SettingsController
 * @package Lar\LteAdmin\Extend\Settings\Controllers
 */
class SettingsController extends Controller
{
    /**
     * @var string
     */
    static $model = LteSettings::class;

    /**
     * @var array[]
     */
    protected $table_column = [
        'input' => ['str_limit', [50]],
        'textarea' => ['str_limit', [50]],
        'password' => ['password_stars', []],
        'number' => ['badge_number', []],
        'file' => ['uploaded_file', []],
        'switcher' => ['on_off', []],
        'icon' => ['fa_icon', []],
        'color' => ['color_cube', []],
        'tags' => ['badge_tags', []],
    ];

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|Sheet
     */
    public function index()
    {
        return Sheet::create(function (ModelTable $table) {
            if (admin()->isRoot()) {
                $table->column('lte-settings.key', 'key')->sort()->copied();
            }
            $table->column('lte-settings.description', 'description')->sort();
            $table->column('lte-settings.value', [$this, 'show_value'])->sort('value');
            if (admin()->isRoot()) {
                $table->column('lte-settings.active', 'active')->input_switcher()->sort();
            }
            $table->updated_at();
            $table->controlEdit($this->can('edit'));
            $table->controlDelete($this->can('delete'));
        });
    }

    /**
     * @return Matrix
     */
    public function matrix()
    {
        return Matrix::create(function (Form $form, Card $card) {

            $card->defaultTools(function ($type) { return $this->can($type); });

            if ($this->isType('create')) {

                $form->select('input', 'lte-settings.field')
                    ->options(LteSettings::getInputs())
                    ->icon_keyboard()
                    ->required();

                $form->input('key', 'lte-settings.key')->icon_key()
                    ->slugable()->required()
                    ->unique(LteSettings::class, 'key', $this->model()->id)->rule(function ($attribute, $value, $fail) {
                        if (config($value)) { $fail('The key has already been taken in configs.');}
                    });

                $form->input('description', 'lte-settings.description')
                    ->required();

                $form->switcher('active', 'lte-settings.active')
                    ->default(1)->boolean();

            } else if ($this->isType('edit')) {

                if (admin()->isRoot()) {

                    $form->select('input', 'lte-settings.field')
                        ->options(LteSettings::getInputs())
                        ->icon_keyboard()
                        ->required();

                    $form->input('description', 'lte-settings.description')
                        ->required();
                }

                if ($this->model()->input && Field::has($this->model()->input)) {

                    $form->{$this->model()->input}('value', $this->model()->description);
                }

                if (admin()->isRoot()) {

                    $form->switcher('active', 'lte-settings.active')
                        ->default(1)->boolean();
                }
            }
        });
    }

    /**
     * @return Info
     */
    public function show()
    {
        return Info::create(function (ModelInfoTable $table, Card $card) {
            $card->defaultTools(function ($type) { return $this->can($type); });

            $table->id();
            $table->row('lte-settings.field', 'input')->to_prepend('lte-settings.inputs.')->to_lang();
            $table->row('lte-settings.key', 'key')->copied();
            $table->row('lte-settings.description', 'description');
            $table->row('lte-settings.value', [$this, 'show_value']);
            $table->row('lte-settings.active', 'active');
            $table->at();
        });
    }

    /**
     * @param  LteSettings  $settings
     * @return false|mixed|string
     */
    public function show_value(LteSettings $settings)
    {
        if (isset($this->table_column[$settings->input]) && ModelTable::hasExtension($this->table_column[$settings->input][0])) {

            return ModelTable::callExtension($this->table_column[$settings->input][0], [
                $settings->value, $this->table_column[$settings->input][1]
            ]);
        }

        return is_array($settings->value) ? json_encode($settings->value) : $settings->value;
    }
}
