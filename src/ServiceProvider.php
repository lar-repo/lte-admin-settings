<?php

namespace Lar\LteAdmin\Extend\Settings;

use Illuminate\Console\Command;
use Lar\LteAdmin\Core\ConfigExtensionProvider;
use Lar\LteAdmin\Extend\Settings\Extension\Config;
use Lar\LteAdmin\Extend\Settings\Models\LteSettings;
use Lar\LteAdmin\ExtendProvider;
use Lar\LteAdmin\Interfaces\NavigateInterface;
use Lar\LteAdmin\Extend\Settings\Extension\Install;
use Lar\LteAdmin\Extend\Settings\Extension\Navigator;
use Lar\LteAdmin\Extend\Settings\Extension\Uninstall;
use Lar\LteAdmin\Extend\Settings\Extension\Permissions;

/**
 * Class ServiceProvider
 * @package Lar\LteAdmin\Extend\Settings
 */
class ServiceProvider extends ExtendProvider
{
    /**
     * @var string
     */
    public static $name = "lar/lte-admin-settings";

    /**
     * @var string
     */
    public static $description = "@lte-settings.menu_title";

    /**
     * @var string
     */
    protected $navigator = Navigator::class;

    /**
     * @var string
     */
    protected $install = Install::class;

    /**
     * @var string
     */
    protected $uninstall = Uninstall::class;

    /**
     * @var string
     */
    protected $permissions = Permissions::class;

    /**
     * @var ConfigExtensionProvider|string
     */
    protected $config = Config::class;

    /**
     * Bootstrap services.
     *
     * @return void
     * @throws \Exception
     */
    public function boot()
    {
        parent::boot();

        /**
         * Apply all configs
         */
        if (\Schema::hasTable('lte_settings')) {
            foreach (LteSettings::whereActive(1)->get(['key', 'value']) as $item) {
                config([$item->key => $item->value]);
            }
        }
    }
}

