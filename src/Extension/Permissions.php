<?php

namespace Lar\LteAdmin\Extend\Settings\Extension;

use Lar\LteAdmin\Core\PermissionsExtensionProvider;
use Lar\LteAdmin\Extend\Settings\Controllers\SettingsController;

/**
 * Class Permissions
 * @package Lar\LteAdmin\Extend\Settings\Extension
 */
class Permissions extends PermissionsExtensionProvider {

    /**
     * Has function permission in extension
     *
     * @return array
     */
    public function functions(): array
    {
        return [
            SettingsController::generatePermission('edit', ['root', 'admin']),
            SettingsController::generatePermission('create', ['root']),
            SettingsController::generatePermission('destroy', ['root']),
        ];
    }
}