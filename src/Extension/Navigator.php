<?php

namespace Lar\LteAdmin\Extend\Settings\Extension;

use Lar\LteAdmin\Core\NavigatorExtensionProvider;
use Lar\LteAdmin\Extend\Settings\Controllers\SettingsController;
use Lar\LteAdmin\Interfaces\ActionWorkExtensionInterface;

/**
 * Class Navigator
 * @package Lar\LteAdmin\Extend\Settings
 */
class Navigator extends NavigatorExtensionProvider implements ActionWorkExtensionInterface {

    /**
     * @return void
     */
    public function handle(): void
    {
        $this->navigate->item('lte-settings.menu_title', 'lte_settings')
            ->resource('lte_settings', '\\' . SettingsController::class)
            ->icon_cog();
    }
}