<?php

namespace Lar\LteAdmin\Extend\Settings\Extension;

use Lar\LteAdmin\Core\InstallExtensionProvider;
use Lar\LteAdmin\Interfaces\ActionWorkExtensionInterface;

/**
 * Class Navigator
 * @package Lar\LteAdmin\Extend\Settings
 */
class Install extends InstallExtensionProvider implements ActionWorkExtensionInterface {

    /**
     * @return void
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function handle(): void
    {
        $this->publish(__DIR__ . '/../../translations', resource_path('lang'));

        $this->migrate(__DIR__ . '/../../migrations');
    }
}