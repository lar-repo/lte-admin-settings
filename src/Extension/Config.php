<?php

namespace Lar\LteAdmin\Extend\Settings\Extension;

use Lar\LteAdmin\Core\ConfigExtensionProvider;

/**
 * Class Config
 * @package Lar\LteAdmin\Extend\LteAdminChat\Extension
 */
class Config extends ConfigExtensionProvider {

    /**
     * On boot lte application
     */
    public function boot()
    {

    }
}