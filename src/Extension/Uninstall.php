<?php

namespace Lar\LteAdmin\Extend\Settings\Extension;

use Lar\LteAdmin\Core\UnInstallExtensionProvider;
use Lar\LteAdmin\Interfaces\ActionWorkExtensionInterface;

/**
 * Class Navigator
 * @package Lar\LteAdmin\Extend\Settings
 */
class Uninstall extends UnInstallExtensionProvider implements ActionWorkExtensionInterface {

    /**
     * @return void
     */
    public function handle(): void
    {
        $this->migrateRollback(__DIR__ . '/../../migrations');

        $this->unpublish(__DIR__ . '/../../translations', resource_path('lang'));
    }
}