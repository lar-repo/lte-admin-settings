<?php

namespace Lar\LteAdmin\Extend\Settings\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LteSettings
 *
 * @package Lar\LteAdmin\Extend\Settings\Models
 * @property int $id
 * @property string $input
 * @property string $key
 * @property mixed $value
 * @property string|null $description
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\Settings\Models\LteSettings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\Settings\Models\LteSettings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\Settings\Models\LteSettings query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\Settings\Models\LteSettings whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\Settings\Models\LteSettings whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\Settings\Models\LteSettings whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\Settings\Models\LteSettings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\Settings\Models\LteSettings whereInput($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\Settings\Models\LteSettings whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\Settings\Models\LteSettings whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Lar\LteAdmin\Extend\Settings\Models\LteSettings whereValue($value)
 * @mixin \Eloquent
 */
class LteSettings extends Model {

    /**
     * @var string
     */
    protected $table = "lte_settings";

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var int[]
     */
    protected $attributes = [
        'active' => 1
    ];

    /**
     * @var string[]
     */
    public static $inputs = [
        'input', 'password', 'email', 'number', 'file', 'switcher', 'date',
        'time', 'icon', 'color', 'select_tags', 'ckeditor', 'textarea'
    ];

    /**
     * @return array
     */
    public static function getInputs()
    {
        return collect(static::$inputs)->mapWithKeys(function ($item) {
            return [$item => __("lte-settings.inputs.{$item}")];
        })->toArray();
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getValueAttribute($value)
    {
        if ($this->input === 'tags') {

            return json_decode($value);
        }

        return $value;
    }

    /**
     * @param $value
     */
    public function setValueAttribute($value)
    {
        if ($this->input === 'select_tags') {

            $value = json_encode($value);
        }

        $this->attributes['value'] = $value;
    }
}