<?php

namespace Lar\LteAdmin\Extend\Settings\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LteSettings
 * @package Lar\LteAdmin\Extend\Settings\Models
 */
class LteSettingCategories extends Model {

    /**
     * @var string
     */
    protected $table = "lte_setting_categories";

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var int[]
     */
    protected $attributes = [
        'active' => 1
    ];
}