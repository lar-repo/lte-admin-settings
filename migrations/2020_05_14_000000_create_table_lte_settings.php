<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLteSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lte_settings', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('input');

            $table->string('key')->unique();

            $table->text('value')->nullable();

            $table->string('description')->nullable();

            $table->boolean('active')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lte_settings');
    }

    /**
     * Ignore down if condition true
     *
     * @return bool
     */
    public function ignore()
    {
        return !!\DB::table('lte_settings')->count();
    }
}
