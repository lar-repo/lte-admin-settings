<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLteSettingsCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lte_setting_categories', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('slug')->unique();

            $table->string('icon');

            $table->string('name');

            $table->integer('order')->default('0');

            $table->boolean('active')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lte_setting_categories');
    }

    /**
     * Ignore down if condition true
     *
     * @return bool
     */
    public function ignore()
    {
        return !!\DB::table('lte_setting_categories')->count();
    }
}
